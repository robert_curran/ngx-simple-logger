# NgxSimpleLogger

NgxSimpleLogger is a logging module for Angular. It provides a simple interface for logging messages to the console
with configurable log levels, intended to allow different logging configuration for development and production environments.

## Dependencies
 * @angular/common
 * @angular/core

## Installation

The module is available via `npm`

```shell
npm install --save ngx-simple-logger
```

## Setup

To use NgxSimpleLogger it must be configured in your applications main module:

```typescript
import { NgxSimpleLoggerModule } from "ngx-simple-logger";

@NgModule({
    imports: [
        NgxSimpleLoggerModule.forRoot()
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

## Use

To use the logger inject it via dependency injection into any component, then call on of the logger functions:

```typescript
import { Component } from "@angular/core";
import { NgxSimpleLoggerService } from "ngx-simple-logger";

@Component({...})
export class SomeComponent {
    constructor(private logger: NgxSimpleLoggerService) { }

    someMethod() {
        this.logger.error("An error has occurred!");
    }
}
```

## Options

The log level option allows configuration for which log level will be written to the console, by default only errors are written.

Only log levels higher or equal to the configured level will be seen. The following log levels are supported:

```
DEBUG|INFO|LOG|WARN|ERROR|OFF
```

The log level is configuration when the NgxSimpleLoggerModule is imported. In this way it is possible to set environment specific log levels utilising the Angular CLI:

```typescript
import { NgxSimpleLoggerModule, LogLevel } from "ngx-simple-logger";
import { environment } from "../environments/environment";

@NgModule({
    imports: [
        NgxSimpleLoggerModule.forRoot({
            logLevel: environment.logLevel
        }),
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
```

## Example Output

<div align="">
    <img src="https://s3.amazonaws.com/ngx-simple-logger/ngx-simple-logger-example.png" />
</div>

## Demo App

A demo app illustrating how to use NgxSimpleLogger is included in the `src/` directory, this can be run with `ng serve`.
