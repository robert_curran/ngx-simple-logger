import { LogLevel } from "ngx-simple-logger";

export const environment = {
    production: true,
    logLevel: LogLevel.ERROR
}
