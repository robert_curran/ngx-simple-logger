import { Component, OnInit } from "@angular/core";
import { NgxSimpleLoggerService } from "ngx-simple-logger";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit {
    title = "ngx-simple-logger";

    constructor(private logger: NgxSimpleLoggerService) { }

    ngOnInit(): void {
        this.logger.debug("This is a debug message from ngx-simple-logger.");
        this.logger.info("This is an info message from ngx-simple-logger.");
        this.logger.log("This is a log message from ngx-simple-logger.");
        this.logger.warn("This is a warning message from ngx-simple-logger.");
        this.logger.error("This is an error message from ngx-simple-logger.");
    }
}
