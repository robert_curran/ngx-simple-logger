import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { NgxSimpleLoggerModule } from "ngx-simple-logger";
describe("AppComponent", () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxSimpleLoggerModule.forRoot()
            ],
            declarations: [
                AppComponent
            ],
        }).compileComponents();
    }));
    it("should create the app", async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it(`should have as title 'ngx-simple-logger'`, async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual("ngx-simple-logger");
    }));
    it("should render title in a h1 tag", async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector("h1").textContent).toContain("Welcome to ngx-simple-logger");
    }));
});
