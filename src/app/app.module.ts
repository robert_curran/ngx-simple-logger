import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgxSimpleLoggerModule, LogLevel } from "ngx-simple-logger";
import { AppComponent } from "./app.component";
import { environment } from "../environments/environment";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        NgxSimpleLoggerModule.forRoot({
            logLevel: environment.logLevel
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
