export enum LogLevel {
    DEBUG = 0, INFO, LOG, WARN, ERROR, OFF
}
