import { Injectable, Inject } from "@angular/core";
import { CONFIG, DEFAULT_CONFIG } from "./ngx-simple-logger.module";
import { NgxSimpleLoggerConfig } from "./interfaces/ngx-simple-logger-config";
import { LogLevel } from "./types/log-level.enum";

@Injectable({
    providedIn: "root"
})
export class NgxSimpleLoggerService {
    /**
     * Logger config
     */
    private config: NgxSimpleLoggerConfig;

    /**
     * NgxSimpleLoggerService constructor
     *
     * @param rootConfig Config provided in module .forRoot() to override defaults
     * @param defaultConfig Default config provided by module
     */
    constructor(
        @Inject(CONFIG) private rootConfig: NgxSimpleLoggerConfig,
        @Inject(DEFAULT_CONFIG) private defaultConfig: NgxSimpleLoggerConfig
    ) {
        this.config = Object.assign({}, defaultConfig, rootConfig);
    }

    /**
     * Getter for debug, binds console.debug
     */
    get debug(): Function {
        if (LogLevel.DEBUG >= this.config.logLevel) {
            return console.debug.bind(console);
        }

        return this.noop;
    }

    /**
     * Getter for info, binds console.info
     */
    get info(): Function {
        if (LogLevel.INFO >= this.config.logLevel) {
            return console.info.bind(console);
        }

        return this.noop;
    }

    /**
     * Getter for log, binds console.log
     */
    get log(): Function {
        if (LogLevel.LOG >= this.config.logLevel) {
            return console.log.bind(console);
        }

        return this.noop;
    }

    /**
     * Getter for warn, binds console.warn
     */
    get warn(): Function {
        if (LogLevel.WARN >= this.config.logLevel) {
            return console.warn.bind(console);
        }

        return this.noop;
    }

    /**
     * Getter for error, binds console.error
     */
    get error(): Function {
        if (LogLevel.ERROR >= this.config.logLevel) {
            return console.error.bind(console);
        }

        return this.noop;
    }

    /**
     * No operation function
     */
    private noop: Function = (): any => undefined;
}
