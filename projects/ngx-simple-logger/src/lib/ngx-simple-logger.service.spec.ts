import { TestBed, inject } from "@angular/core/testing";

import { NgxSimpleLoggerService } from "./ngx-simple-logger.service";
import { NgxSimpleLoggerModule } from "./ngx-simple-logger.module";
import { LogLevel } from "./types/log-level.enum";

describe("NgxSimpleLoggerService", () => {
    let service: NgxSimpleLoggerService;

    describe("service configurations", () => {
        it("should created the service with default (ERROR) log level", () => {
            TestBed.configureTestingModule({
                imports: [
                    NgxSimpleLoggerModule.forRoot()
                ],
                providers: [
                    NgxSimpleLoggerService
                ]
            });

            service = TestBed.get(NgxSimpleLoggerService);

            expect(service).toBeTruthy();

            spyOn(console, "error");
            spyOn(console, "warn");
            service.error("error");
            service.warn("warning");
            expect(console.error).toHaveBeenCalledWith("error");
            expect(console.warn).not.toHaveBeenCalled();
        });

        it("should created the service with the log level configured in the module", () => {
            TestBed.configureTestingModule({
                imports: [
                    NgxSimpleLoggerModule.forRoot({
                        logLevel: LogLevel.LOG
                    })
                ],
                providers: [
                    NgxSimpleLoggerService
                ]
            });

            service = TestBed.get(NgxSimpleLoggerService);

            expect(service).toBeTruthy();

            spyOn(console, "log");
            spyOn(console, "info");
            service.log("log");
            service.info("info");
            expect(console.log).toHaveBeenCalledWith("log");
            expect(console.info).not.toHaveBeenCalled();
        });
    });

    describe("logging functions", () => {
        const testMessage = "This is a test message";

        describe("LogLevel.DEBUG", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.DEBUG
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should write to console.debug when LogLevel is DEBUG", () => {
                spyOn(console, "debug");
                service.debug(testMessage);
                expect(console.debug).toHaveBeenCalledWith(testMessage);
            });
        });

        describe("LogLevel.INFO", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.INFO
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should not write to console.debug when LogLevel is INFO", () => {
                spyOn(console, "debug");
                service.debug(testMessage);
                expect(console.debug).not.toHaveBeenCalled();
            });

            it("should write to console.info when LogLevel is INFO", () => {
                spyOn(console, "info");
                service.info(testMessage);
                expect(console.info).toHaveBeenCalledWith(testMessage);
            });
        });

        describe("LogLevel.LOG", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.LOG
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should not write to console.info when LogLevel is LOG", () => {
                spyOn(console, "info");
                service.info(testMessage);
                expect(console.info).not.toHaveBeenCalled();
            });

            it("should write to console.log when LogLevel is LOG", () => {
                spyOn(console, "log");
                service.log(testMessage);
                expect(console.log).toHaveBeenCalledWith(testMessage);
            });
        });

        describe("LogLevel.WARN", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.WARN
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should not write to console.log when LogLevel is WARN", () => {
                spyOn(console, "log");
                service.log(testMessage);
                expect(console.log).not.toHaveBeenCalled();
            });

            it("should write to console.warn when warnLevel is WARN", () => {
                spyOn(console, "warn");
                service.warn(testMessage);
                expect(console.warn).toHaveBeenCalledWith(testMessage);
            });
        });

        describe("LogLevel.ERROR", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.ERROR
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should not write to console.warn when warnLevel is ERROR", () => {
                spyOn(console, "warn");
                service.warn(testMessage);
                expect(console.warn).not.toHaveBeenCalled();
            });

            it("should write to console.error when errorLevel is ERROR", () => {
                spyOn(console, "error");
                service.error(testMessage);
                expect(console.error).toHaveBeenCalledWith(testMessage);
            });
        });

        describe("LogLevel.OFF", () => {
            beforeEach(() => {
                TestBed.configureTestingModule({
                    imports: [
                        NgxSimpleLoggerModule.forRoot({
                            logLevel: LogLevel.OFF
                        })
                    ],
                    providers: [
                        NgxSimpleLoggerService
                    ]
                });

                service = TestBed.get(NgxSimpleLoggerService);
            });

            it("should not write to console.error when errorLevel is OFF", () => {
                spyOn(console, "error");
                service.error(testMessage);
                expect(console.error).not.toHaveBeenCalled();
            });
        });
    });
});
