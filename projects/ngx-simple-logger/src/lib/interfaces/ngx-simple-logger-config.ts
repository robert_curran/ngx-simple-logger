import { LogLevel } from "../types/log-level.enum";

export interface NgxSimpleLoggerConfig {
    logLevel: LogLevel;
}
