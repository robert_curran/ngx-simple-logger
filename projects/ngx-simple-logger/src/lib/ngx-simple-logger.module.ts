import { NgModule, ModuleWithProviders, InjectionToken, Optional } from "@angular/core";
import { NgxSimpleLoggerConfig } from "./interfaces/ngx-simple-logger-config";
import { LogLevel } from "./types/log-level.enum";

export const CONFIG: InjectionToken<NgxSimpleLoggerConfig> =
    new InjectionToken<NgxSimpleLoggerConfig>("SimpleLoggerConfig");

export const DEFAULT_CONFIG: InjectionToken<NgxSimpleLoggerConfig> =
    new InjectionToken<NgxSimpleLoggerConfig>("SimpleLoggerConfig");

@NgModule({
    providers: [
        {
            provide: DEFAULT_CONFIG,
            useValue: { logLevel: LogLevel.ERROR }
        }
    ]
})
export class NgxSimpleLoggerModule {
    static forRoot(config?: NgxSimpleLoggerConfig): ModuleWithProviders {
        return {
            ngModule: NgxSimpleLoggerModule,
            providers: [
                {
                    provide: CONFIG,
                    useValue: config
                }
            ]
        }
    }
}
