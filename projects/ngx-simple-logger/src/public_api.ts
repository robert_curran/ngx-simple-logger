/*
 * Public API Surface of ngx-simple-logger
 */
export { NgxSimpleLoggerModule } from "./lib/ngx-simple-logger.module";
export { NgxSimpleLoggerService } from "./lib/ngx-simple-logger.service";
export { LogLevel } from "./lib/types/log-level.enum";
